# Copyright (C) 2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import logging
import os
import os.path
import sys

import cliapp

from . import hosts


class LocalDownstream(hosts.DownstreamHost):
    @staticmethod
    def add_app_settings(app_settings):
        app_settings.string(
            ['local-base-directory'],
            'Base directory for local Downstream Host')

    @staticmethod
    def check_app_settings(app_settings):
        if not app_settings['local-base-directory']:
            logging.error('A base directory must be provided to create '
                          'repositories on a local filesystem.')
            app_settings.require('local-base-directory')

    def __init__(self, app_settings):
        self._base_dir = app_settings['local-base-directory']

    def prepare_repo(self, repo_path, metadata):
        repo_path = '%s/%s.git' % (self._base_dir, repo_path)

        # These are idempotent, so we don't need to explicitly check
        # whether the repository already exists
        os.makedirs(repo_path, exist_ok=True)
        cliapp.runcmd(
            ['git', 'init', '--bare',
             '--template',
             os.path.join(sys.prefix, 'share/lorry-controller/git-templates'),
             repo_path])

        if 'head' in metadata:
            cliapp.runcmd(['git', '--git-dir', repo_path,
                           'symbolic-ref', 'HEAD',
                           'refs/heads/' + metadata['head']])
        if 'description' in metadata:
            with open(os.path.join(repo_path, 'description'), 'w') as f:
                print(metadata['description'], file=f)
