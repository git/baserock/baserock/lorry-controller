# Copyright (C) 2015,2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import logging

import cliapp

from . import hosts


class GerritDownstream(hosts.DownstreamHost):
    '''Run commands on a Gerrit instance.

    This uses the SSH API to Gerrit. The REST API is actually much nicer to
    use, but it requires that the account doing stuff has set an HTTP password.
    Use of the SSH API requires only the SSH key that we also use for push
    access.

    '''

    @staticmethod
    def add_app_settings(app_settings):
        app_settings.string(
            ['gerrit-parent-project'],
            'parent project for repositories on Gerrit',
            default='All-Projects',
            metavar='PROJECT')

    @staticmethod
    def check_app_settings(app_settings):
        if app_settings['downstream-visibility'] != 'private':
            raise cliapp.ApplicationError(
                'Cannot create non-private repositories in Gerrit')

    def __init__(self, app_settings):
        url = app_settings['downstream-ssh-url']
        if url is None:
            url = 'ssh://lorry@localhost:29418'
            key_check = 'no'
        else:
            key_check = 'yes'
        self._ssh_command = hosts.SshCommand(
            url, StrictHostKeyChecking=key_check)

        self._parent_project = app_settings['gerrit-parent-project']

    def _has_project(self, name):
        # There's no 'does this project exist' command in Gerrit 2.9.4; 'list
        # all projects with this prefix' is as close we can get.

        output = self._ssh_command.run([
            'gerrit', 'ls-projects', '--type=ALL', '--prefix=%s' % name])
        projects = output.strip().split('\n')

        if name in projects:
            return True
        else:
            return False

    def prepare_repo(self, name, metadata):
        '''Create a project in the local Gerrit server.

        The 'lorry' user must have createProject capability in the Gerrit.

        '''

        if self._has_project(name):
            logging.info('Project %s exists in local Gerrit already.',
                         name)

            # We can only set this metadata if we're the owner of the
            # repository.  For now, ignore failures.
            try:
                if 'head' in metadata:
                    self._ssh_command.run(['gerrit', 'set-head', name,
                                           '--new-head', metadata['head']])
                if 'description' in metadata:
                    self._ssh_command.run(['gerrit', 'set-project', name,
                                           '-d', metadata['description']])
            except cliapp.AppException:
                pass
        else:
            create_args = ['gerrit', 'create-project', name,
                           '-p', self._parent_project]
            if 'head' in metadata:
                create_args.extend(['-b', metadata['head']])
            if 'description' in metadata:
                create_args.extend(['-d', metadata['description']])
            self._ssh_command.run(create_args)
            logging.info('Created %s project in local Gerrit.', name)
